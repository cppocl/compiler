/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../compiler.hpp"

TEST(OCL_COMPILER_DEFINED)
{
    const bool exists =
#ifdef OCL_COMPILER
        true;
#else
        false;
#endif

    CHECK_TRUE(exists);
}

TEST(OCL_DEBUG_OR_RELEASE_DEFINED)
{
    const bool debug_or_release_defined =
#if defined(OCL_DEBUG) || defined(OCL_RELEASE)
        true;
#else
        false;
#endif

    CHECK_TRUE(debug_or_release_defined);
}

TEST(OCL_COMPILER_VERSION_DEFINED)
{
    const bool version_defined =
#ifdef OCL_COMPILER_VERSION
        true;
#else
        false;
#endif

    CHECK_TRUE(version_defined);
}

TEST(OCL_OVERRIDE_DEFINED)
{
    const bool override_defined =
#ifdef OCL_OVERRIDE
        true;
#else
        false;
#endif

    CHECK_TRUE(override_defined);
}

TEST(OCL_NULLPTR_DEFINED)
{
    const bool nullptr_defined =
#ifdef OCL_NULLPTR
        true;
#else
        false;
#endif

    CHECK_TRUE(nullptr_defined);
}

TEST(OCL_COMPILER_POSIX_SUPPORT_DEFINED)
{
    const bool posix_support_defined =
#ifdef OCL_COMPILER_POSIX_SUPPORT
        true;
#else
        false;
#endif
    CHECK_TRUE(posix_support_defined);
}
