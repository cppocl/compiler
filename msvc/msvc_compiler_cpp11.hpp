/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_CPP11_HPP
#define OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_CPP11_HPP

/// Define the various C++11 keywords and specifiers or equivalents.
///

#include "msvc_compiler_check.hpp"

#if (OCL_COMPILER_YEAR >= 2015)

    #define OCL_CONSTEXPR constexpr
    #define OCL_NOEXCEPT noexcept

#else // Older than VS2015

    #define OCL_CONSTEXPR
    #define OCL_NOEXCEPT throw()

#endif // #else (OCL_COMPILER_YEAR >= 2015)

#if (OCL_COMPILER_YEAR >= 2010)

    #define OCL_OVERRIDE override
    #define OCL_NULLPTR nullptr

#else

    #define OCL_OVERRIDE
    #define OCL_NULLPTR NULL

#endif // #else (OCL_COMPILER_YEAR >= 2010)


/// Language support.
///

#if (OCL_COMPILER_YEAR >= 2012)
#define OCL_SUPPORT_MOVE_SEMANTICS 1
#define OCL_SUPPORT_TYPED_ENUM 1
#else
#define OCL_SUPPORT_MOVE_SEMANTICS 0
#define OCL_SUPPORT_TYPED_ENUM 0
#endif

#if (OCL_COMPILER_YEAR >= 2015)
#define OCL_CPP11_FULL_SUPPORT 1
#else
#define OCL_CPP11_FULL_SUPPORT 0
#endif

#endif // OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_CPP11_HPP
