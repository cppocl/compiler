/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_TYPES_HPP
#define OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_TYPES_HPP

#include "msvc_compiler_check.hpp"

#ifndef OCL_COMPILER_YEAR
#error Compiler year undefined!
#endif

#ifndef OCL_WORD_SIZE
#error Word size not defined!
#endif

#if OCL_COMPILER_YEAR >= 2012

#include <cstdint>

#elif OCL_COMPILER_YEAR >= 2010

#include <stdint.h>

#else

#include <stddef.h>

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int int16_t;
typedef unsigned short int uint16_t;
typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef signed __int64 int64_t;
typedef unsigned __int64 uint64_t;

#if (OCL_WORD_SIZE >= 64)
    typedef int64_t ptrdiff_t;
    typedef int64_t intptr_t;
    typedef uint64_t uintptr_t;
#else
    typedef int32_t ptrdiff_t;
    typedef int32_t intptr_t;
    typedef uint32_t uintptr_t;
#endif

#endif

#endif // OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_TYPES_HPP
