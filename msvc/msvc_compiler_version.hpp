/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_VERSION_HPP
#define OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_VERSION_HPP

#include "msvc_compiler_check.hpp"

#if defined(OCL_COMPILER_VERSION_MAJOR) || defined(OCL_COMPILER_VERSION_MINOR) || defined(OCL_COMPILER_VERSION_PATCH)
#error Compiler version already defined!
#endif

#define OCL_COMPILER_VERSION_MAJOR (_MSC_VER / 100)
#define OCL_COMPILER_VERSION_MINOR (_MSC_VER % 100)
#define OCL_COMPILER_VERSION_PATCH 0
#define OCL_COMPILER_VERSION ((OCL_COMPILER_VERSION_MAJOR * 10000) + OCL_COMPILER_VERSION_MINOR + OCL_COMPILER_VERSION_PATCH)
#define OCL_COMPILER_NATIVE_VERSION _MSC_VER

#if _MSC_VER >= 1920
#define OCL_COMPILER_YEAR 2019
#elif _MSC_VER >= 1910
#define OCL_COMPILER_YEAR 2017
#elif _MSC_VER >= 1900
#define OCL_COMPILER_YEAR 2015
#elif _MSC_VER >= 1800
#define OCL_COMPILER_YEAR 2013
#elif _MSC_VER >= 1700
#define OCL_COMPILER_YEAR 2012
#elif _MSC_VER >= 1600
#define OCL_COMPILER_YEAR 2010
#elif _MSC_VER >= 1500
#define OCL_COMPILER_YEAR 2008
#elif _MSC_VER >= 1400
#define OCL_COMPILER_YEAR 2005
#elif _MSC_VER >= 1310
#define OCL_COMPILER_YEAR 2003
#elif _MSC_VER >= 1300
#define OCL_COMPILER_YEAR 2002
#else
// Older Microsoft compilers don't have the year defined.
#define OCL_COMPILER_YEAR 0
#endif

#endif // OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_VERSION_HPP
