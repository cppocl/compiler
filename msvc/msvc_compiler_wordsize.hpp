/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_WORDSIZE_HPP
#define OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_WORDSIZE_HPP

#include "msvc_compiler_check.hpp"

#if defined(OCL_WORD_SIZE)
#error Microsoft word size already defined!
#endif

#if defined(_M_X64) || defined(_WIN64)
#define OCL_WORD_SIZE 64
#elif defined(_WIN32)
#define OCL_WORD_SIZE 32
#else
#error Unknown word size!
#endif

#endif // OCL_GUARD_COMPILER_MSVC_MSVC_COMPILER_WORDSIZE_HPP
