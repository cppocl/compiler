/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_INTERNAL_COMPILER_COMPILERS_HPP
#define OCL_GUARD_COMPILER_INTERNAL_COMPILER_COMPILERS_HPP

#if !defined(__cplusplus)
#error C++ compiler required!
#endif

// List of fully supported or identified C/C++ compilers.
#define OCL_COMPILER_MSVC 1
#define OCL_COMPILER_GCC 2
#define OCL_COMPILER_MINGW 3
#define OCL_COMPILER_CYGWIN 4
#define OCL_COMPILER_CLANG 5
#define OCL_COMPILER_BORLAND 6
#define OCL_COMPILER_DIAB 7
#define OCL_COMPILER_DIGITAL_MARS 8
#define OCL_COMPILER_DIGNUS_SYSTEMS 9
#define OCL_COMPILER_GREEN_HILLS 10
#define OCL_COMPILER_IAR 11
#define OCL_COMPILER_IBM_XL 12
#define OCL_COMPILER_IBM_ZOS 13
#define OCL_COMPILER_METROWERKS 14
#define OCL_COMPILER_WATCOM 15

#endif // OCL_GUARD_COMPILER_INTERNAL_COMPILER_COMPILERS_HPP
