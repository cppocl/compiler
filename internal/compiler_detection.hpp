/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_INTERNAL_COMPILER_DETECTION_HPP
#define OCL_GUARD_COMPILER_INTERNAL_COMPILER_DETECTION_HPP

/*
    This header file only deals with detecting the compiler and defining identified and supported compilers.

    When OCL_COMPILER is defined, the library has detected the compiler.

    The name of the compiler is defined as OCL_COMPILER_AUTHOR.

    Whatever compiler define is detected will be defined as OCL_COMPILER_???_DEFINE,
    e.g. OCL_COMPILER_MSVC_DEFINE for Microsoft Visual C++.
*/

#if !defined(__cplusplus)
#error C++ compiler required!
#endif

// Include the list of supported compilers.
#include "compilers.hpp"

#if defined(OCL_COMPILER) || defined(OCL_COMPILER_DEFINE)
#error Compiler already detected!
#endif

// Microsoft Visual Studio C/C++ compiler for Windows.
#ifdef _MSC_VER
#define OCL_COMPILER_DEFINE _MSC_VER
#define OCL_COMPILER_AUTHOR "Microsoft"
#define OCL_COMPILER OCL_COMPILER_MSVC
#endif

// gcc compiler for most platforms (except Windows)
#if !defined(OCL_COMPILER)
#if defined(__GNUC__)
#define OCL_COMPILER_DEFINE __GNUC__
#define OCL_COMPILER_AUTHOR "GCC"
#define OCL_COMPILER OCL_COMPILER_GCC
#endif
#endif

// Windows minimal port of gcc compiler under GPL license or commercial license.
// NOTE: Provides limited Posix support.
#if !defined(OCL_COMPILER)
#if defined(__MINGW__)
#define OCL_COMPILER_DEFINE __MINGW__
#elif defined(__MINGW32__)
#define OCL_COMPILER_DEFINE __MINGW32__
#endif

#if defined(OCL_COMPILER_DEFINE)
#define OCL_COMPILER OCL_COMPILER_MINGW
#define OCL_COMPILER_AUTHOR "MinGW"
#endif
#endif

// Windows port of gcc compiler with minimal compiler support.
// NOTE: Provides extensive Posix support.
#if !defined(OCL_COMPILER)
#if defined(__CYGWIN__)
#define OCL_COMPILER_DEFINE __CYGWIN__
#elif defined(__CYGWIN32__)
#define OCL_COMPILER_CYGWIN_DEFINE __CYGWIN32__
#endif

#if !defined(OCL_COMPILER_DEFINE)
#define OCL_COMPILER OCL_COMPILER_CYGWIN
#define OCL_COMPILER_AUTHOR "Cygwin"
#endif
#endif

// LLVM project C/C++ compiler for Clang.
#if !defined(OCL_COMPILER)
#if defined(__llvm__)
#define OCL_COMPILER_DEFINE __llvm__
#elif defined(__clang__)
#define OCL_COMPILER_DEFINE __clang__
#endif

#if !defined(OCL_COMPILER_DEFINE)
#define OCL_COMPILER OCL_COMPILER_CLANG
#define OCL_COMPILER_AUTHOR "Clang"
#endif
#endif

#if !defined(OCL_COMPILER)
#if defined(__BORLANDC__)
#define OCL_COMPILER_DEFINE __BORLANDC__
#define OCL_COMPILER OCL_COMPILER_BORLAND
#define OCL_COMPILER_AUTHOR "Borland"
#endif
#endif

// Diab C/C++
#if !defined(OCL_COMPILER)
#if defined(__DCC__)
#define OCL_COMPILER_DEFINE __DCC__
#define OCL_COMPILER OCL_COMPILER_DIAB
#define OCL_COMPILER_AUTHOR "WindRiver"
#endif
#endif

// Digital Mars (previously Symantec and Zortech). Version: VRP = Version Revision Patch
#if !defined(OCL_COMPILER)
#if defined(__DMC__)
#define OCL_COMPILER_DEFINE __DMC__
#define OCL_COMPILER OCL_COMPILER_DIGITAL_MARS
#define OCL_COMPILER_AUTHOR "Digital Mars"
#endif
#endif

// Dignus Systems/C and Systems/C++ (for IBM mainframes). Version: VRRPP = Version Revision Patch. e.g. 1.90.00 = 19000
#if !defined(OCL_COMPILER)
#if defined(__SYSC__)
#define OCL_COMPILER_DEFINE __SYSC__
#define OCL_COMPILER OCL_COMPILER_DIGNUS_SYSTEMS
#define OCL_COMPILER_AUTHOR "Dignus"
#endif
#endif

// Green Hills C/C++. Version: VRP = Version Revision Patch
#if !defined(OCL_COMPILER)
#if defined(__ghs__)
#define OCL_COMPILER_DEFINE __ghs__
#define OCL_COMPILER OCL_COMPILER_GREEN_HILLS
#define OCL_COMPILER_AUTHOR "Green Hills"
#endif
#endif

// IAR C/C++. Version: VRR = Version Revision
#if !defined(OCL_COMPILER)
#if defined(__IAR_SYSTEMS_ICC__)
#define OCL_COMPILER_DEFINE __IAR_SYSTEMS_ICC__
#define OCL_COMPILER OCL_COMPILER_IAR
#define OCL_COMPILER_AUTHOR "IAR"
#endif
#endif

// IBM XL C/C++. Version: NVRRP = Product Version Revision Patch. e.g. 41070
#if !defined(OCL_COMPILER)
#if defined(__xlc__)
#define OCL_COMPILER_DEFINE __xlc__
#elif defined(__xlC__)
#define OCL_COMPILER_DEFINE __xlC__
#endif

// If detected set the current compiler.
#if defined(OCL_COMPILER_DEFINE)
#define OCL_COMPILER OCL_COMPILER_IBM_XL
#define OCL_COMPILER_AUTHOR "IBM XL"
#endif
#endif

// IBM z/OS C/C++. Version: NVRRP = Product Version Revision Patch. e.g. 41070
#if !defined(OCL_COMPILER)
#if defined(__IBMC__)
#define OCL_COMPILER_DEFINE __IBMC__
#elif defined(__IBMCPP__)
#define OCL_COMPILER_DEFINE __IBMCPP__
#endif

// If detected set the current compiler.
#if defined(OCL_COMPILER_DEFINE)
#define OCL_COMPILER OCL_COMPILER_IBM_ZOS
#define OCL_COMPILER_AUTHOR "IBM z/OS"
#endif
#endif

// Metrowerks (now Freescale) Codewarrior C/C++ compiler. Version: 0xVRPP = Version Revision Patch. e.g. 2.2 = 0x2200
#if !defined(OCL_COMPILER)
#if defined(__MWERKS__)
#define OCL_COMPILER_DEFINE __MWERKS__
#define OCL_COMPILER OCL_COMPILER_METROWERKS
#define OCL_COMPILER_AUTHOR "Metrowerks"
#endif
#endif

// Watcom C/C++
#if !defined(OCL_COMPILER)
#if defined(__WATCOMC__)
#define OCL_COMPILER_WATCOM_DEFINE __WATCOMC__
#define OCL_COMPILER OCL_COMPILER_WATCOM
#define OCL_COMPILER_AUTHOR "Watcom"
#endif
#endif

#endif // OCL_GUARD_COMPILER_INTERNAL_COMPILER_DETECTION_HPP
