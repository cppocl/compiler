/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_COMPILER_HPP
#define OCL_GUARD_COMPILER_COMPILER_HPP

#if !defined(__cplusplus)
#error C++ compiler required!
#endif

#include "internal/compiler_detection.hpp"

#ifndef OCL_COMPILER
#error Compiler not supported!
#endif

// The order of these includes is important in building the defines for the compiler support.

#if OCL_COMPILER == OCL_COMPILER_MSVC

#define OCL_COMPILER_POSIX_SUPPORT 0

#include "msvc/msvc_compiler_debug.hpp"
#include "msvc/msvc_compiler_version.hpp"
#include "msvc/msvc_compiler_cppxx.hpp"
#include "msvc/msvc_compiler_widechar.hpp"
#include "msvc/msvc_compiler_wordsize.hpp"
#include "msvc/msvc_compiler_types.hpp"

#elif OCL_COMPILER == OCL_COMPILER_MINGW

#define OCL_COMPILER_POSIX_SUPPORT 1

#include "mingw/mingw_compiler_debug.hpp"
#include "mingw/mingw_compiler_version.hpp"
#include "mingw/mingw_compiler_cppxx.hpp"
#include "mingw/mingw_compiler_widechar.hpp"
#include "mingw/mingw_compiler_wordsize.hpp"
#include "mingw/mingw_compiler_types.hpp"

#elif OCL_COMPILER == OCL_COMPILER_CYGWIN

#define OCL_COMPILER_POSIX_SUPPORT 1

#include "cygwin/cygwin_compiler_debug.hpp"
#include "cygwin/cygwin_compiler_version.hpp"
#include "cygwin/cygwin_compiler_cppxx.hpp"
#include "cygwin/cygwin_compiler_widechar.hpp"
#include "cygwin/cygwin_compiler_wordsize.hpp"
#include "cygwin/cygwin_compiler_types.hpp"

#elif OCL_COMPILER == OCL_COMPILER_GCC

#define OCL_COMPILER_POSIX_SUPPORT 1

#include "gcc/gcc_compiler_debug.hpp"
#include "gcc/gcc_compiler_version.hpp"
#include "gcc/gcc_compiler_cppxx.hpp"
#include "gcc/gcc_compiler_widechar.hpp"
#include "gcc/gcc_compiler_wordsize.hpp"
#include "gcc/gcc_compiler_types.hpp"

#else

#error Compiler not supported!

#endif

#endif // OCL_GUARD_COMPILER_COMPILER_HPP
