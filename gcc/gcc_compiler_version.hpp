/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_GCC_GCC_COMPILER_VERSION_HPP
#define OCL_GUARD_COMPILER_GCC_GCC_COMPILER_VERSION_HPP

#include "gcc_compiler_check.hpp"

#if defined(OCL_COMPILER_VERSION) || defined(OCL_COMPILER_VERSION_MAJOR) || defined(OCL_COMPILER_VERSION_MINOR) || defined(OCL_COMPILER_VERSION_PATCH) || defined(OCL_COMPILER_YEAR)
#error GNU GCC veriosn already defined!
#endif

#define OCL_COMPILER_VERSION ((__GNUC__ * 10000) + (__GNUC_MINOR__ * 100) + __GNUC_PATCHLEVEL__)
#define OCL_COMPILER_VERSION_MAJOR __GNUC__
#define OCL_COMPILER_VERSION_MINOR __GNUC_MINOR__
#define OCL_COMPILER_VERSION_PATCH __GNUC_PATCHLEVEL__
#define OCL_COMPILER_NATIVE_VERSION __GNUC__

// Year not defined for GCC compiler.
#define OCL_COMPILER_YEAR 0

#endif // OCL_GUARD_COMPILER_GCC_GCC_COMPILER_VERSION_HPP
