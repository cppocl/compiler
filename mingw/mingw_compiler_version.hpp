/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#ifndef OCL_GUARD_COMPILER_MINGW_MINGW_COMPILER_VERSION_HPP
#define OCL_GUARD_COMPILER_MINGW_MINGW_COMPILER_VERSION_HPP

#include "mingw_compiler_check.hpp"

#ifdef OCL_COMPILER_VERSION
#error MinGW compiler version already defined!
#endif

#if defined(__MINGW32__)
#define OCL_COMPILER_VERSION __MINGW32__
#define OCL_COMPILER_NATIVE_VERSION __MINGW32__
#elif defined(__MINGW__)
#define OCL_COMPILER_VERSION __MINGW__
#define OCL_COMPILER_NATIVE_VERSION __MINGW32__
#endif

// Year not defined for MINGW compiler.
#define OCL_COMPILER_YEAR 0

#endif // OCL_GUARD_COMPILER_MINGW_MINGW_COMPILER_VERSION_HPP
