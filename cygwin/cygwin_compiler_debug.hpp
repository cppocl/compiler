/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_DEBUG_HPP
#define OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_DEBUG_HPP

#include "cygwin_compiler_check.hpp"

#ifdef NDEBUG

#ifdef OCL_RELEASE
#error OCL_RELEASE already defined!
#endif

#ifdef OCL_DEBUG
#error Cannot define OCL_DEBUG and OCL_RELEASE together!
#endif

#define OCL_RELEASE

#else

#ifdef OCL_DEBUG
#error OCL_DEBUG already defined!
#endif

#ifdef OCL_RELEASE
#error Cannot define OCL_DEBUG and OCL_RELEASE together!
#endif

#define OCL_DEBUG

#endif // ifdef NDEBUG

#endif // OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_DEBUG_HPP
