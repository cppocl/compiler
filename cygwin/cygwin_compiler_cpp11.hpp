/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_CPP11_HPP
#define OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_CPP11_HPP

#include "cygwin_compiler_check.hpp"

// TODO: Support not known.
#define OCL_CPP11_FULL_SUPPORT 0

namespace ocl
{
    constexpr bool cpp11_full_supported =
#if defined(OCL_CPP11_FULL_SUPPORT) && (OCL_CPP11_FULL_SUPPORT == 1)
        true;
#else
        false;
#endif
}

#endif // OCL_GUARD_COMPILER_CYGWIN_CYGWIN_COMPILER_CPP11_HPP
